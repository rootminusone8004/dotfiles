# history
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zsh_history

unsetopt menu_complete
unsetopt flowcontrol

setopt prompt_subst
setopt always_to_end
setopt append_history
setopt auto_menu
setopt complete_in_word
setopt extended_history
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_verify
setopt inc_append_history
setopt share_history
setopt no_list_ambiguous

source ~/.aliases

# plugins
source ~/.config/zsh/powerlevel10k/powerlevel10k.zsh-theme
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source ~/.config/zsh/zsh-auto-notify/auto-notify.plugin.zsh
source ~/.config/zsh/zsh-you-should-use/you-should-use.plugin.zsh

# bindkeys
bindkey '^[j' history-substring-search-up
bindkey '^[k' history-substring-search-down

if [ -f "$HOME/.custom_paths" ]; then
	source "$HOME/.custom_paths"
fi

export CHROME_EXECUTABLE="brave"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
