#!/usr/bin/perl

# obmenu-generator - schema file

=for comment

    item:      add an item inside the menu               {item => ["command", "label", "icon"]},
    cat:       add a category inside the menu             {cat => ["name", "label", "icon"]},
    sep:       horizontal line separator                  {sep => undef}, {sep => "label"},
    pipe:      a pipe menu entry                         {pipe => ["command", "label", "icon"]},
    file:      include the content of an XML file        {file => "/path/to/file.xml"},
    raw:       any XML data supported by Openbox          {raw => q(...)},
    beg:       begin of a category                        {beg => ["name", "icon"]},
    end:       end of a category                          {end => undef},
    obgenmenu: generic menu settings                {obgenmenu => ["label", "icon"]},
    exit:      default "Exit" action                     {exit => ["label", "icon"]},

=cut

# NOTE:
#    * Keys and values are case sensitive. Keep all keys lowercase.
#    * ICON can be a either a direct path to an icon or a valid icon name
#    * Category names are case insensitive. (X-XFCE and x_xfce are equivalent)

require "$ENV{HOME}/.config/obmenu-generator/config.pl";

## Text editor
my $editor = $CONFIG->{editor};

our $SCHEMA = [

    #          COMMAND                 LABEL              ICON
    #{item => ['pcmanfm',       	  'File Manager', 'system-file-manager']},
    {item => ['brave',     'brave',      'brave']},
    {item => ['terminator',       'Terminator',   'terminator']},
    {item => ['codium',    'vscode',    'code']},
    # {item => ['gmrun',            'Run command',  'system-run']},
    
    {sep => 'Categories'},

    #          NAME            LABEL                ICON
    {cat => ['utility',     'Accessories', 'applications-utilities']},
    {cat => ['development', 'Development', 'applications-development']},
    {cat => ['education',   'Education',   'applications-science']},
    {cat => ['game',        'Games',       'applications-games']},
    {cat => ['graphics',    'Graphics',    'applications-graphics']},
    {cat => ['audiovideo',  'Multimedia',  'applications-multimedia']},
    {cat => ['network',     'Network',     'applications-internet']},
    {cat => ['office',      'Office',      'applications-office']},
    {cat => ['WebApps',     'Web Apps',    'webapp-manager']},
    # {beg => ['Kali tools', 'kali-menu']},
    #     {beg => ['info gathering',         'kali-info-gathering-trans']},
		# 	    {cat => ['01-01-dns-analysis',           'dns-analysis',         'kali-info-gathering-trans']},
    #       {cat => ['01-02-identify-live-hosts',    'host identification',  'kali-info-gathering-trans']},
    #       {cat => ['01-03-ids-ips-identification', 'firewall tools',       'kali-info-gathering-trans']},
    #       {cat => ['01-04-network-scanners',       'network-scanners',     'kali-info-gathering-trans']},
    #       {cat => ['01-07-osint-analysis',         'osint-analysis',       'kali-info-gathering-trans']},
    #       {cat => ['01-08-route-analysis',         'route-analysis',       'kali-info-gathering-trans']},
    #       {cat => ['01-10-smb-analysis',           'smb-analysis',         'kali-info-gathering-trans']},
    #       {cat => ['01-11-smtp-analysis',          'smtp-analysis',        'kali-info-gathering-trans']},
    #       {cat => ['01-12-snmp-analysis',          'snmp-analysis',        'kali-info-gathering-trans']},
    #       {cat => ['01-13-ssl-analysis',           'ssl-analysis',         'kali-info-gathering-trans']},          
		# 	    {cat => ['01-info-gathering',            'defaults',             'kali-info-gathering-trans']},
    #     {end => undef},
    #     {beg => ['vulnerability analysis',         'kali-vuln-assessment-trans']},
    #       {cat => ['02-01-cisco-tools',            'cisco-tools',          'kali-vuln-assessment-trans']},
    #       {cat => ['02-02-fuzzers',                'fuzzers',              'kali-vuln-assessment-trans']},
    #       {cat => ['02-03-voip-tools',             'voip-tools',           'kali-vuln-assessment-trans']},
    #       {cat => ['02-05-nessus',                 'nessus',               'kali-vuln-assessment-trans']},
    #       {cat => ['02-06-openvas',                'openvas',              'kali-vuln-assessment-trans']},
    #       {cat => ['02-07-stress-testing',         'stress-testing',       'kali-vuln-assessment-trans']},
    #       {cat => ['02-vulnerability analysis',    'defaults',             'kali-vuln-assessment-trans']},
    #     {end => undef},
    #     {beg => ['webapp-analysis',         'kali-web-application-trans']},        
    #       {cat => ['03-01-cms-identification',         'cms-identification',          'kali-web-application-trans']},
    #       {cat => ['03-04-web-crawlers',               'web-crawlers',                'kali-web-application-trans']},
    #       {cat => ['03-05-web-vulnerability-scanners', 'web-vulnerability-scanners',  'kali-web-application-trans']},
    #       {cat => ['03-06-web-application-proxies',    'webapp-proxies',              'kali-web-application-trans']},
    #       {cat => ['03-webapp-analysis',               'defaults',                    'kali-web-application-trans']},          
    #     {end => undef},
    #     {cat => ['04-database-assessment',    'database-assessment',    'kali-database-assessment-trans']},
    #     {beg => ['password attacks',    'kali-password-attacks-trans']},
    #       {cat => ['05-01-online-attacks',      'online-attacks',    'kali-password-attacks-trans']},
    #       {cat => ['05-02-offline-attacks',     'offline-attacks',   'kali-password-attacks-trans']},
    #       {cat => ['05-04-pass-hash',           'pass-hash',         'kali-password-attacks-trans']},
    #       {cat => ['05-05-profile',             'password-profile',  'kali-password-attacks-trans']},
    #       {cat => ['05-password-attacks',       'defaults',          'kali-password-attacks-trans']},          
    #     {end => undef},
    #     {beg => ['wireless attacks',    'kali-wireless-attacks-trans']},
    #       {cat => ['06-01-wireless-tools',      'wireless-tools',    'kali-wireless-attacks-trans']},
    #       {cat => ['06-02-bluetooth-tools',     'bluetooth-tools',   'kali-wireless-attacks-trans']},
    #       {cat => ['06-03-rfid-nfc-tools',      'rfid-nfc-tools',    'kali-wireless-attacks-trans']},
    #       {cat => ['06-04-other-wireless',      'other-tools',       'kali-wireless-attacks-trans']},
    #       {cat => ['06-05-radio-tools',         'radio-tools',       'kali-wireless-attacks-trans']},
    #       {cat => ['06-wireless-attacks',       'defaults',          'kali-wireless-attacks-trans']},          
    #     {end => undef},
    #     {cat => ['07-reverseengineer',        'reverse engineer',       'kali-reverse-engineering-trans']},
    #     {cat => ['08-exploitation-tools',     'exploitation tools',     'kali-exploitation-tools-trans']},
    #     {beg => ['sniffing spoofing',    'kali-sniffing-spoofing-trans']},
    #       {cat => ['09-01-network-sniffers',    'network-sniffers', 'kali-sniffing-spoofing-trans']},
    #       {cat => ['09-02-network-spoofing',    'network-spoofing', 'kali-sniffing-spoofing-trans']},
    #       {cat => ['09-sniffing-spoofing',      'defaults',         'kali-sniffing-spoofing-trans']},          
    #     {end => undef},
    #     {beg => ['maintaining access',    'kali-maintaining-access-trans']},
    #       {cat => ['10-01-os-backdoors',        'os-backdoors',       'kali-maintaining-access-trans']},
    #       {cat => ['10-02-tunneling',           'tunneling',          'kali-maintaining-access-trans']},
    #       {cat => ['10-03-web-backdoors',       'web-backdoors',      'kali-maintaining-access-trans']},
    #       {cat => ['10-04-command-control',     'command-control',    'kali-maintaining-access-trans']},
    #       {cat => ['10-maintaining-access',     'defaults',           'kali-maintaining-access-trans']},          
    #     {end => undef},
    #     {beg => ['forensics',    'kali-forensics-trans']},
    #       {cat => ['11-03-digital-forensics',      'digital-forensics',        'kali-forensics-trans']},
    #       {cat => ['11-04-forensic-analysis',      'forensic-analysis',        'kali-forensics-trans']},
    #       {cat => ['11-05-forensic-carving-tools', 'forensic-carving-tools',   'kali-forensics-trans']},
    #       {cat => ['11-06-forensic-hashing-tools', 'forensic-hashing-tools',   'kali-forensics-trans']},
    #       {cat => ['11-07-forensic-imaging-tools', 'forensic-imaging-tools',   'kali-forensics-trans']},
    #       {cat => ['11-08-forensic-suites',        'forensic-suites',          'kali-forensics-trans']},
    #       {cat => ['11-11-pdf-forensics-tools',    'pdf-forensics-tools',      'kali-forensics-trans']},
    #       {cat => ['11-forensics',                 'defaults',                 'kali-forensics-trans']},          
    #     {end => undef},
    #     {cat => ['12-reporting',              'reporting',              'kali-reporting-tools-trans']},
    #     {cat => ['13-social-engineering-tools', 'social engineering',   'kali-social-engineering-trans']},
    #     {beg => ['services',    'kali-system-services-trans']},
    #       {cat => ['14-08-beef-service',        'beef',         'kali-system-services-trans']},
    #       {cat => ['14-09-metasploit-service',  'metasploit',   'kali-system-services-trans']},
    #       {cat => ['14-10-dradis-service',      'dradis',       'kali-system-services-trans']},
    #       {cat => ['14-11-openvas-service',     'openvas',      'kali-system-services-trans']},
    #       {cat => ['14-12-xplico-service',      'xplico',       'kali-system-services-trans']},
    #       {cat => ['14-13-gvm-service',         'gvm',          'kali-system-services-trans']},
    #       {cat => ['14-14-faraday-service',     'faraday',      'kali-system-services-trans']},
    #       {cat => ['14-services',               'defaults',     'kali-system-services-trans']},
    #     {end => undef},
    #     {cat => ['15-kali-offsec-links',        'kali-offsec-links',            'kali-menu']},
    #   {end => undef},
    # {cat => ['other',       'Other',       'applications-other']},
    {cat => ['settings',    'Settings',    'applications-accessories']},
    {cat => ['system',      'System',      'applications-system']},

    #             LABEL          ICON
    #{beg => ['My category',  'cat-icon']},
    #          ... some items ...
    #{end => undef},

    #            COMMAND     LABEL        ICON
    #{pipe => ['obbrowser', 'Disk', 'drive-harddisk']},

    ## Generic advanced settings
    #{sep       => undef},
    #{obgenmenu => ['Openbox Settings', 'applications-engineering']},
    #{sep       => undef},

    ## Custom advanced settings
    {sep => undef},
    {beg => ['Advanced Settings', 'applications-engineering']},

      # Configuration files
      {item => ["$editor ~/.conkyrc",              'Conky RC',    'text-x-generic']},
      {item => ["$editor ~/.config/tint2/tint2rc", 'Tint2 Panel', 'text-x-generic']},

      # obmenu-generator category
      {beg => ['Obmenu-Generator', 'accessories-text-editor']},
        {item => ["$editor ~/.config/obmenu-generator/schema.pl", 'Menu Schema', 'text-x-generic']},
        {item => ["$editor ~/.config/obmenu-generator/config.pl", 'Menu Config', 'text-x-generic']},

        {sep  => undef},
        {item => ['obmenu-generator -s -c',    'Generate a static menu',             'accessories-text-editor']},
        {item => ['obmenu-generator -s -i -c', 'Generate a static menu with icons',  'accessories-text-editor']},
        {sep  => undef},
        {item => ['obmenu-generator -p',       'Generate a dynamic menu',            'accessories-text-editor']},
        {item => ['obmenu-generator -p -i',    'Generate a dynamic menu with icons', 'accessories-text-editor']},
        {sep  => undef},

        {item => ['obmenu-generator -d', 'Refresh cache', 'view-refresh']},
      {end => undef},

      # Openbox category
      {beg => ['Openbox', 'openbox']},
        {item => ["$editor ~/.config/openbox/autostart", 'Openbox Autostart',   'text-x-generic']},
        {item => ["$editor ~/.config/openbox/rc.xml",    'Openbox RC',          'text-x-generic']},
        {item => ["$editor ~/.config/openbox/menu.xml",  'Openbox Menu',        'text-x-generic']},
        {item => ['openbox --reconfigure',               'Reconfigure Openbox', 'openbox']},
      {end => undef},
    {end => undef},

    {sep => undef},

    ## The xscreensaver lock command
    {item => ['slock', 'Lock', 'system-lock-screen']},

    ## This option uses the default Openbox's "Exit" action
    {exit => ['Exit', 'application-exit']},

    ## This uses the 'oblogout' menu
    # {item => ['oblogout', 'Exit', 'application-exit']},
]
